## Install kernels to a [WinPython](http://winpython.github.io/) Jupyter installation

The installation is portable and can be copied to usb drives etc.

The kernels are installed by running the notebooks from this repository.

